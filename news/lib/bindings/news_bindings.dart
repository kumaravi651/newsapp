import 'package:get/get.dart';
import 'package:news/controllers/network_connectivity.dart';
import 'package:news/controllers/news_controller.dart';

class NewsBinding extends Bindings {
  void dependencies() {
    Get.lazyPut<NewsController>(
      () => NewsController(),
    );
    Get.lazyPut<NetworkConnectivity>(
      () => NetworkConnectivity(),
    );
  }
}
