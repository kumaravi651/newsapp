import 'package:flutter/material.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'package:get/get.dart';
import 'package:news/constants/category.dart';
import 'package:news/controllers/network_connectivity.dart';
import 'package:news/controllers/news_controller.dart';
import 'package:news/widgets/article_card.dart';
import 'package:news/widgets/category_tile.dart';
import 'package:news/widgets/check_internet_connection.dart';
import 'package:news/widgets/launch_url.dart';

class TopHeadlines extends GetView<NewsController> {
  @override
  Widget build(BuildContext context) {
    final NewsController _newsController = Get.find();

    return Column(
      children: [
        Container(
            width: Get.width,
            height: 100,
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: Category.categorylist.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (ctx, index) {
                  return InkWell(
                    onTap: () {
                      _newsController.category.value =
                          Category.categorylist[index]["name"];
                      _newsController
                          .fetchTopHeadlines(_newsController.category);
                    },
                    child: CategoryTile(
                        image: Category.categorylist[index]["image"],
                        categoryName: Category.categorylist[index]["name"]),
                  );
                })),
        Expanded(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 3, vertical: 3),
          child: GetBuilder<NetworkConnectivity>(builder: (state) {
            return (state.isOnline == null)
                ? const Center(
                    child: const CircularProgressIndicator(),
                  )
                : state.isOnline
                    ? Container(
                        child: Obx(() {
                          if (_newsController.isLoading.value)
                            return const Center(
                              child: const CircularProgressIndicator(),
                            );
                          else
                            return ListView.builder(
                              itemCount: _newsController.newsList.length,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () => launchURL(context,
                                      _newsController.newsList[index].url),
                                  child: ArticleCard(
                                    imageUrl: _newsController
                                        .newsList[index].urlToImage,
                                    title:
                                        _newsController.newsList[index].title,
                                    source: _newsController
                                        .newsList[index].source.name,
                                    publishedAt: _newsController
                                        .newsList[index].publishedAt,
                                  ),
                                );
                              },
                            );
                        }),
                      )
                    : SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: CheckInternetConnection(
                          image: 'lottie/no_internet_lottie.json',
                          title: 'Network Error',
                          description: 'Internet not found !!',
                          buttonText: "Retry",
                          onPressed: () {
                            controller
                                .fetchTopHeadlines(NewsController().category);
                          },
                        ),
                      );
          }),
        )),
      ],
    );
  }
}
