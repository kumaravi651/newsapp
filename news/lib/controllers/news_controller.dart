import 'package:get/get.dart';
import 'package:news/services/remote_services.dart';

class NewsController extends GetxController {
  var newsList = [].obs;
  var articleListForQuery = [].obs;
  var sourceList = [].obs;
  var isLoading = true.obs;
  var isSourcesLoading = true.obs;
  var isResultsForQueryLoading = true.obs;
  var category = "general".obs;
  var pos = 0.obs;
  var query = "".obs;

  @override
  void onInit() {
    fetchTopHeadlines(category);

    super.onInit();
  }

  void fetchTopHeadlines(RxString category) async {
    try {
      isLoading(true);
      var articles = await RemoteServices.fetchTopHeadlines(category.string);

      if (articles != null) {
        newsList.assignAll(articles);
        update();
      }
    } catch (e) {} finally {
      isLoading(false);
    }
  }
}
