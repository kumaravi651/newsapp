import 'package:get/get.dart';
import 'package:news/bindings/news_bindings.dart';
import 'package:news/screens/home_page.dart';

import 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: Paths.HOME,
      page: () => HomePage(),
      binding: NewsBinding(),
    ),
  ];
}
