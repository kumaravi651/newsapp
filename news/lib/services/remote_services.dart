import 'package:dio/dio.dart';
import 'package:news/constants/data.dart';
import 'package:news/models/news_response.dart';
import 'dart:convert';

class RemoteServices {
  static var client = Dio();

  static Future<List<Articles>> fetchTopHeadlines(String category) async {
    var url =
        '${Data.BASE_URL}/v2/top-headlines?country=us&category=$category&apiKey=${Data.API}';
    final response = await client.get(url);
    if (response.statusCode == 200) {
      var jsonString = response.data;

      var articles = NewsResponse.fromJson(jsonString).articles;
      return articles;
    } else {
      return null;
    }
  }
}
